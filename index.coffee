module.exports =
  pg:
    live:
      name:     "live"
      host:     "search1.zoomsquare.com"
      database: "zoomsquare_new_realestateservice"
      user:     "zoomsquare"
      password_env: "ZS_PG_PASSWORD"

    test: # deprecated
      name:     "test"
      host:     "postgres.tp.zoomsquare.com"
      database: "zoomsquare_new_realestateservice"
      user:     "zoomsquare"
      password_env: "ZS_PG_PASSWORD"

    "postgres.tp":
      name: "test"
      host: "postgres.tp.zoomsquare.com"
      database: "zoomsquare_new_realestateservice"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_POSTGRES_TP"

    "tp.index":
      name: "tp.index"
      host: "postgres.tp.zoomsquare.com"
      database: "zoomsquare_new_realestateservice"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_POSTGRES_TP"

    "tp.zs": # deprecated
      name:     "tp.zs"
      host:     "postgres.tp.zoomsquare.com"
      database: "zoomsquare"
      user:     "zoomsquare"
      password_env: "ZS_PG_PASSWORD"

    "postgres.tp.zoomsquare":
      name: "tp.zs"
      host: "postgres.tp.zoomsquare.com"
      database: "zoomsquare"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD"
#    search2:
#      name:     "search2"
#      host:     "search2.zoomsquare.com"
#      database: "zoomsquare_new_realestateservice"
#      user:     "zoomsquare"
#      password_env: "ZS_PG_PASSWORD_SEARCH2"
    search2test:
      name:     "search2test"
      host:     "search2.zoomsquare.com"
      database: "zoomsquare_test"
      user:     "zoomsquare"
      password_env: "ZS_PG_PASSWORD_INDEX_AT"
    oraculum:
      name:     "oraculum"
      host:     "postgres.tp.zoomsquare.com"
      database: "oraculum"
      user:     "zoomsquare"
      password_env: "ZS_PG_PASSWORD_ORACULUM"

#    index:
#      name:     "index"
#      host:     "index.zoomsquare.com"
#      database: "zoomsquare"
#      user:     "zoomsquare"
#      password_env: "ZS_PG_PASSWORD_INDEX"

    "at.index":
      name: "at.index"
      host: "de.index.zoomsquare.com"
      database: "zoomsquare_at"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_INDEX_DE"

    "at.index_pipe":
      name: "at.index_pipe"
      host: "/tmp"
      database: "zoomsquare_at"
      user: "zoomsquare"

    "de.index":
      name: "de.index"
      host: "de.index.zoomsquare.com"
      database: "zoomsquare"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_INDEX_DE"

    "de.index_pipe":
      name: "de.index_pipe"
      host: "/tmp"
      database: "zoomsquare"
      user: "zoomsquare"

    "de.index_test":
      name: "de.index_test"
      host: "de.index.zoomsquare.com"
      database: "zoomsquare_test"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_INDEX_DE"

    "bdm2":
      name: "bdm2"
      host: "bdm2.zoomsquare.com"
      database: "bdm"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_BDM2"

    "at.index.de_test":
      name: "at.index.de_test"
      host: "at.index.zoomsquare.com"
      database: "zoomsquare_de_test"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_INDEX_AT"

    "userservice":
      name: "userservice"
      host: "userservice.zoomsquare.com"
      database: "userservice"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_USERSERVICE"

    "userservice_local":
      name: "userservice_local"
      host: "localhost"
      database: "userservice"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_USERSERVICE"

    "userservice_dev_local":
      name: "userservice",
      host: "localhost",
      database: "userservice",
      user: "zoomsquare"

    pipeline1:
      name: "pipeline1"
      host: "pipeline1.zoomsquare.com"
      database: "bdm"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_BDM"

    pipeline2:
      name: "pipeline2"
      host: "pipeline2.zoomsquare.com"
      database: "bdm"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_BDM"

    es1:
      name: "es1"
      host: "es1.zoomsquare.com"
      database: "bdm"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_BDM"

    es2:
      name: "es2"
      host: "es2.zoomsquare.com"
      database: "bdm"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_BDM"

    "local.bdm":
      name: "local.bdm"
      host: "localhost"
      database: "bdm"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_BDM"

    "local.bdm_master":
      name: "local.bdm_master"
      host: "localhost"
      database: "bdm_master"
      user: "zoomsquare"
      password_env: "ZS_PG_PASSWORD_BDM"

    local:
      name:     "local"
      host:     "localhost"
      database: "zoomsquare"
      user:     "zoomsquare"
      password_env: "ZS_PG_PASSWORD_LOCAL"

  mongo:
    live:
      name:     "live"
      host:     process.env.ZS_MONGO_LIVE_HOST or 'mongodb0.zoomsquare.com,mongodb1.zoomsquare.com,mongodb2.zoomsquare.com'
      database: "zoomsquare"
      user:     "zoomsquare"
      password_env: "ZS_MONGO_PASSWORD"
    next:
      name:     "next"
      host:     'next.zoomsquare.com'
      database: "zoomsquare"
      user:     "zoomsquare"
      password_env: "ZS_MONGO_NEXT_PASSWORD"
    test:
      name:     "test"
      host:     'mongodb.tp.zoomsquare.com'
      database: "admin"
      user:     "piffie"
      password_env: "ZS_MONGO_PASSWORD"
    backend:
      name:     "backend"
      host:     'mongodbbe0.zoomsquare.com'
      database: "backend?replicaSet=zoomsquare_backend&authSource=admin"
      user:     "zoomsquare"
      password_env: "ZS_MONGO_BACKEND_PASSWORD"

  redis:
    logging:
      name:     "logging"
      host:     "logging.zoomsquare.com"
      port:     6379
      password_env: "ZS_REDIS_LOGGING_PASSWORD"
      database: 0

    crawler1:
      name:     "crawler1"
      host:     "crawler1.zoomsquare.com"
      sock:     process.env.ZS_REDIS_CRAWLER1_SOCK
      port:     6379
      password_env: "ZS_REDIS_CRAWLER1_PASSWORD"
      database: 0

    crawler1_1:
      name: "crawler1_1"
      host: "crawler1.zoomsquare.com"
      sock: process.env.ZS_REDIS_CRAWLER1_SOCK
      port: 6379
      password_env: "ZS_REDIS_CRAWLER1_PASSWORD"
      database: 1

    crawler2:
      name:     "crawler2"
      host:     "crawler2.zoomsquare.com"
      sock:     process.env.ZS_REDIS_CRAWLER2_SOCK
      port:     6379
      password_env: "ZS_REDIS_CRAWLER2_PASSWORD"
      database: 0

    pipeline:
      name:     "pipeline"
      host:     "pipeline1.zoomsquare.com"
      sock:     process.env.ZS_REDIS_PIPELINE_SOCK
      port:     6379
      password_env: "ZS_REDIS_PIPELINE_PASSWORD"
      database: 0

    "pipeline.tp":
      name:     "pipeline.tp"
      host:     "pipeline.tp.zoomsquare.com"
      sock:     process.env.ZS_REDIS_PIPELINE_TP_SOCK
      port:     6379
      password_env: "ZS_REDIS_PIPELINE_TP_PASSWORD"
      database: 0

    localhost:
      name:     "localhost"
      host:     "localhost"
      sock:     process.env.ZS_REDIS_LOCALHOST_SOCK
      port:     6379
      password_env: "ZS_REDIS_LOCALHOST_PASSWORD"
      database: 0

    "de.index":
      name:     "de.index"
      host:     "de.index.zoomsquare.com"
      sock:     process.env.ZS_REDIS_DE_INDEX_SOCK
      port:     6379
      password_env: "ZS_REDIS_DE_INDEX_PASSWORD"
      database: 0

    "next":
      name:     "next"
      host:     "next.zoomsquare.com"
      sock:     process.env.ZS_REDIS_NEXT_SOCK
      port:     6379
      password_env: "ZS_REDIS_NEXT_PASSWORD"
      database: 0

    "www1":
      name:     "www1"
      host:     "www1.zoomsquare.com"
      sock:     process.env.ZS_REDIS_WWW1_SOCK
      port:     6379
      password_env: "ZS_REDIS_WWW1_PASSWORD"
      database: 0

    "www2":
      name:     "www2"
      host:     "www2.zoomsquare.com"
      sock:     process.env.ZS_REDIS_WWW2_SOCK
      port:     6379
      password_env: "ZS_REDIS_WWW2_PASSWORD"
      database: 0

    "utils":
      name:     "utils"
      host:     "utils.zoomsquare.com"
      sock:     process.env.ZS_REDIS_UTILS_SOCK
      port:     6379
      password_env: "ZS_REDIS_UTILS_PASSWORD"
      database: 0

  rabbitmq:
    utils:
      host:
        username: "zoomsquare"
        password: process.env.ZS_UTILS_RABBIT_MQ_PASSWORD
        queue: "amqp://zoomsquare:#{process.env.ZS_UTILS_RABBIT_MQ_PASSWORD}@utils.zoomsquare.com"
        api: "http://zoomsquare:#{process.env.ZS_UTILS_RABBIT_MQ_PASSWORD}@utils.zoomsquare.com:15672"
        vhost: "%2f"
    "de.index":
      host:
        username: "zoomsquare"
        password: process.env.ZS_RABBITMQ_DE_INDEX_PASSWORD
        queue: "amqp://zoomsquare:#{process.env.ZS_RABBITMQ_DE_INDEX_PASSWORD}@de.index.zoomsquare.com"
        api: "http://zoomsquare:#{process.env.ZS_RABBITMQ_DE_INDEX_PASSWORD}@de.index.zoomsquare.com:15672"
        vhost: "%2f"

  es:
    "res1":
      name: "res1"
      host: "res1.zoomsquare.com"

    "res2":
      name: "res2"
      host: "res2.zoomsquare.com"

    "res3":
      name: "res3"
      host: "res3.zoomsquare.com"

    "res":
      name: "res"
      host: "res.zoomsquare.com"

    "search.tp":
      name: "search.tp"
      host: "search.tp.zoomsquare.com"
      password_env: "ZS_ELASTICSEARCH_SEARCH_TP_PASSWORD"

  immohosts:
    upvote: [
      "www.wohnnet.at"
    ]
    devalue: [
      "immosurf.de",
      "www.wohnungsmarkt24.de",
      "www.wohnungsboerse.net",
      "www.immodirekt.at",
      "de.scutt.eu",
      "de.meega.eu",
      "de.next-immo.com"
    ]

  # use this to override certain fields
  getDBStringFromConfig: (db,conf) ->
    for field in ["password","user","host","database"]
      unless conf[field]
        throw new Error("Missing '#{field}' for db: #{db}")

    switch db
      when "mongo"  then "mongodb://#{conf.user}:#{conf.password}@#{conf.host}/#{conf.database}"
      when "pg"     then "postgres://#{conf.user}:#{conf.password}@#{conf.host}/#{conf.database}"
      when "redis"  then "redis://user:#{conf.password}@#{conf.host}/?db=#{conf.database}"
      else throw new Error("Unknown db: #{db}")

  getDBConfig: (db,lane) ->
    unless @[db]?
      throw new Error("Unknown db #{db}")
    unless @[db][lane]?
      throw new Error("Unknown lane #{lane} for db #{db}")

    config = @[db][lane]
    if not config.password? and config.password_env?
      unless process.env[config.password_env]?
        throw new Error("Missing env. var. '#{config.password_env}' for requested db string #{db}/#{lane}")

      config.password = process.env[config.password_env]
    config

  getDBString: (db,lane) ->
    @getDBStringFromConfig db, @getDBConfig(db,lane)

  endpoints:
    live:
      xtr:
        base:             'http://pipeline1.zoomsquare.com:8080'
        extractFragments:   '/extractFragments'
      "xtr-ng":
        base:             "http://pipeline1.zoomsquare.com:5001"
        getType:            "/getType"
      bdm:
        base:             "http://pipeline1.zoomsquare.com"
        geoApprox:          '/api/geoApprox'
        neighborhood:       "/api/getNeighborhoodScores"
      pipeline: # deprecated, use pipeline.x
        base:             "http://de.index.zoomsquare.com:1332"
        socket:           'http://de.index.zoomsquare.com:1332'

    test:
      xtr:
        base:             'http://xtr.tp.zoomsquare.com:8080'
        extractFragments:   '/extractFragments'
      "xtr-ng":
        base:             'http://xtr.tp.zoomsquare.com:5001'
        getType:            '/getType'
      bdm:
        base:             "http://bdm.tp.zoomsquare.com"
        geoApprox:          '/api/geoApprox'
        neighborhood:       "/api/getNeighborhoodScores"
      pipeline: # deprecated, use pipeline.x
        base:             "http://pipeline.tp.zoomsquare.com:1332"
        socket:           "http://pipeline.tp.zoomsquare.com:1332"

      es:
        base:             "http://search.tp.zoomsquare.com:9200"
        zoomsquare:       "/zoomsquare"
        zoomsquare2:      "/zoomsquare2"

    zoomLive: 'http://api.zoomsquare.com/v1/recrawl'
    debugger: 'http://ticket.zoomsquare.com/debugInterface.html?url='
    oraculum: 'http://tools.zoomsquare.com:1337'

    logging:
      host: "logging.zoomsquare.com"

    pipeline:
      "pipeline.tp":
        lane:             "test" # for pipelineStates
        base:             "http://pipeline.tp.zoomsquare.com:1332"
        socket:           "http://pipeline.tp.zoomsquare.com:1332"
        api:              "http://pipeline.tp.zoomsquare.com:3333/api"
      "at.index":
        lane:             "live" # for pipelineStates
        base:             "http://pipeline1.zoomsquare.com:1333"
        socket:           "http://pipeline1.zoomsquare.com:1333"
        api:              "http://pipeline1.zoomsquare.com:3333/api"
      "de.index":
        lane:             "live" # for pipelineStates
        base:             "http://pipeline2.zoomsquare.com:1333"
        socket:           "http://pipeline2.zoomsquare.com:1333"
        api:              "http://pipeline2.zoomsquare.com:3333/api"
      localhost:
        lane:             "localhost"
        base:             "http://localhost:1332"
        socket:           "http://localhost:1332"
        api:              "http://localhost:3333/api"

    crawler:
      "pipeline.tp.zoomsquare.com":
        base:             "http://pipeline.tp.zoomsquare.com:8080"
        user:             "zoomsquare"
        pass:             process.env.ZS_CRAWLER_PASSWORD
        userpassed:       "http://zoomsquare:#{process.env.ZS_CRAWLER_PASSWORD}@pipeline.tp.zoomsquare.com:8080"
      "crawler1.zoomsquare.com":
        base:             "http://crawler1.zoomsquare.com:8080"
        user:             "zoomsquare"
        pass:             process.env.ZS_CRAWLER_PASSWORD
        userpassed:       "http://zoomsquare:#{process.env.ZS_CRAWLER_PASSWORD}@crawler1.zoomsquare.com:8080"
      "crawler2.zoomsquare.com":
        base:             "http://crawler2.zoomsquare.com:8080"
        user:             "zoomsquare"
        pass:             process.env.ZS_CRAWLER_PASSWORD
        userpassed:       "http://zoomsquare:#{process.env.ZS_CRAWLER_PASSWORD}@crawler2.zoomsquare.com:8080"
      localhost:
        base:             "http://localhost:8080"
        user:             "zoomsquare"
        pass:             process.env.ZS_CRAWLER_PASSWORD
        userpassed:       "http://zoomsquare:#{process.env.ZS_CRAWLER_PASSWORD}@localhost:8080"

    get: (service, endpoint, lane)->
      unless lane? and @[lane]?
        console.log "unknown lane #{lane}"
      unless service? and @[lane][service]?
        console.log "unknown service #{service} in lane #{lane}"
      unless endpoint? and @[lane][service][endpoint]?
        console.log "unknown endpoint #{endpoint} of service #{service} in lane #{lane}"

      @[lane][service].base + @[lane][service][endpoint]
